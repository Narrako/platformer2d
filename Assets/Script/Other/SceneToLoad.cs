﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneToLoad : MonoBehaviour
{
    public void Play()
    {
        SceneManager.LoadScene("Selection Niveau");
    }
    
    public void Parametre()
    {
        SceneManager.LoadScene("Paramètre");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}