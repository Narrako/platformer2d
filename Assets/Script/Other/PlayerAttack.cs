﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public GameObject attack;
    private Bounds _bounds;
    public LayerMask mask;

    private Animator anim;
    
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A)) 
        {
            attack.SetActive(true);
            anim.SetTrigger("Fight"); 
        }

        if (Input.GetKeyUp(KeyCode.A))
            StartCoroutine(DesactiveBox());
    }

    IEnumerator DesactiveBox()
    {
        yield return new WaitForSeconds(1f);
        attack.SetActive(false);
    }
}