﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthEnnemy1 : MonoBehaviour
{
    public GameObject deathParticules;
    public Transform particlesPosition;
    private int health = 1;
    
    public void DealDamage()
    {
        health--;
        
        if(health <=0 )
         StartCoroutine(EnnemyDeath());
    }

    IEnumerator EnnemyDeath()
    {
        GetComponent<MoveEnnemy1>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        Instantiate(deathParticules, particlesPosition.position, particlesPosition.rotation); //ajoute des particles de mort
        yield return new WaitForSeconds(3f); //attends le temps des particles
        Destroy(gameObject);
        yield return null;
    }
}
