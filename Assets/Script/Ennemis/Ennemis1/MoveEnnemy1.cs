﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnnemy1 : MonoBehaviour
{
    public float speed;
    public bool MoveRight;
    public Rigidbody2D _rigidbody2D;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if(MoveRight) //si l'ennmi se déplace vers la droite
        {
            transform.Translate(2 * Time.deltaTime * speed, 0, 0);
            transform.localScale = new Vector3(-0.3f, transform.localScale.y, transform.localScale.z);
        }
        else 
        {
            transform.Translate(-2 * Time.deltaTime * speed, 0, 0);
            transform.localScale = new Vector3(0.3f, transform.localScale.y, transform.localScale.z);
        }
    }

    private void OnCollisionStay2D(Collision2D collider2D)
    {
        if (collider2D.gameObject.CompareTag("Player"))
            _rigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;

        else
            _rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
    }
   
    private void OnTriggerEnter2D(Collider2D collision) //pour le faire changer de sens
    {
        if(collision.gameObject.CompareTag("Turn"))
        {
            if (MoveRight)
                MoveRight = false;
            else
                MoveRight = true;
        }
        
        if (collision.gameObject.CompareTag("Attack"))
        {
            gameObject.GetComponent<HealthEnnemy1>().DealDamage();
        }
    }
}