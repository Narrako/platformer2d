﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoinCountUI : MonoBehaviour
{
    public static CoinCountUI instance;
    public TextMeshProUGUI text;
    private int score;

    void Start()
    {
        if (instance == null)
            instance = this;
    }

    void Update()
    {
        
    }

    public void ChangeScore(int coinValue)
    {
        score += coinValue;
        text.text = "x" + score.ToString();
    }
}
