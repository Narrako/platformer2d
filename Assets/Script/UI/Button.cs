﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public GameObject helpWindow, closeButton, settingWindow, closeSettingButton;
    public Animator animWindow, animSetting;

    public void Start()
    {
        animWindow = helpWindow.GetComponent<Animator>();
        animSetting = settingWindow.GetComponent<Animator>();
    }

    public void Play()
    {
        SceneManager.LoadScene("Selection Niveau");
    }

    public void Help()
    {
        animWindow.SetBool("Open", true);
        closeButton.SetActive(true);
    }

    public void QuitHelp()
    {
        animWindow.SetBool("Open",false);
        closeButton.SetActive(false);
    }

    public void Setting()
    {
        animSetting.SetBool("Open", true);
        closeSettingButton.SetActive(true);
    }

    public void QuitSetting()
    {
        animSetting.SetBool("Open", false);
        closeSettingButton.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
