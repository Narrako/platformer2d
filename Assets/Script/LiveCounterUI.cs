﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(TMP_Text))]
public class LiveCounterUI : MonoBehaviour
{
    [SerializeField] private TMP_Text livesText;

    void Awake()
    {
        livesText = GetComponent<TMP_Text>();
    }

    void Update()
    {
        livesText.text = "x" + GameMaster.ReamaingLives.ToString();
    }
}