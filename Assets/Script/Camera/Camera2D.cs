﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2D : MonoBehaviour
{
    public Transform target;
    public float damping; //a quel point on rend la caméra plus smooth
    public float lookAheadFactor; //permet de voir devant le perso avant de revenir
    public float lookAheadReturnSpeed = 0.5f;
    public float lookAheadMoveThreshold = 0.1f;

    public float yPosRestrictionMin, yPosRestrictionMax, xPosRestrictionMin, xPosRestionctionMax; //les bounds de la caméra
    
    private float offsetZ;
    private Vector3 lastTargetPosition;
    private Vector3 currentVelocity;
    private Vector3 lookAheadPos;

    private float nextTimeToSearch = 0;
    
    void Start()
    {
        lastTargetPosition = target.position;
        offsetZ = (transform.position - target.position).z;
        transform.parent = null;
    }
    
    void Update()
    {
        if (target == null)
        {
            FindPlayer();
            return;
        }
        
        float xMoveDelta = (target.position - lastTargetPosition).x;  //met à jour la position d'anticipation qu'en cas d'accélération ou de changement de direction

        bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

        if (updateLookAheadTarget)
            lookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
        else
            lookAheadPos = Vector3.MoveTowards(lookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);

        Vector3 aheadTargetPos = target.position + lookAheadPos + Vector3.forward * offsetZ;
        Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref currentVelocity, damping);

        newPos = new Vector3(Mathf.Clamp(newPos.x, xPosRestrictionMin, xPosRestionctionMax), Mathf.Clamp(newPos.y, yPosRestrictionMin, yPosRestrictionMax), newPos.z);
        
        transform.position = newPos;

        lastTargetPosition = target.position;
    }

    void FindPlayer()
    {
        if (nextTimeToSearch <= Time.time)
        {
             GameObject searchResult = GameObject.FindGameObjectWithTag("Player");
             if (searchResult != null)
                 target = searchResult.transform;
             nextTimeToSearch = Time.time + 0.5f;
        }
    }
}
