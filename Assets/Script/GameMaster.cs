﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class GameMaster : MonoBehaviour
{
    public static GameMaster gm;
    public Transform playerPrefabs;
    public Transform spawnPoint;
    public int spawnDelay = 2;
    public GameObject spawnPrefabs;

    [SerializeField] private int maxLives = 3;
    private static int _remainingives;

    [SerializeField] private GameObject gameOverUI;
    
    public GameObject deathParticules;
    public Transform particlesPosition;
    
    private void Awake()
    {
        if (gm == null)
        {
            gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameMaster>();
        }
    }
    
    private void Start()
    {
        _remainingives = maxLives;
    }

    public static int ReamaingLives
    {
        get { return _remainingives; }
    }

    public void EndGame()
    {
        //Game Over
        gameOverUI.SetActive(true);
    }

    public IEnumerator RespawnPlayer()
    {
        yield return new WaitForSeconds(spawnDelay);
        
        Instantiate(playerPrefabs, spawnPoint.position, spawnPoint.rotation);
        GameObject clone = Instantiate(spawnPrefabs, spawnPoint.position, spawnPoint.rotation) as GameObject; //ajouter des effets de particule de respawn
        Destroy(clone, 3f);
    }

    public void KillPlayer(Player player)
    {
        particlesPosition = player.GetComponent<Player>().particlesPosition;
        Instantiate(deathParticules, particlesPosition.position, particlesPosition.rotation); //ajoute des particles de mort
      //  StartCoroutine(PlayerDeath());
        Destroy(player.gameObject);
        _remainingives -= 1;
        if (_remainingives <= 0)
        {
            gm.EndGame();
        }
        else
        {
            gm.StartCoroutine(gm.RespawnPlayer());
        }
    }

    IEnumerator PlayerDeath()
    {
        yield return new WaitForSeconds(1.8f);
    }
}
