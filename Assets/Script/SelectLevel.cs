﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectLevel : MonoBehaviour
{
    public int niveau;

    public void OnTriggerStay2D(Collider2D player)
    {
        if (player.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.Return))
                SceneManager.LoadScene(niveau);
        }
    }
}



