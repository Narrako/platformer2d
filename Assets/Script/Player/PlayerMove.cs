﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public PlayerController controller;

    public float runSpeed = 40f;
    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;
    
 //   public AudioSource audioSourceJump, audioSourceWalk;
 //   public AudioClip audioClipJump, audioClipWalk;
    
    void Update () {

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
    //        audioSourceJump.clip = audioClipJump;
    //        audioSourceJump.Play();
        }
    }

    void FixedUpdate ()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, jump); //déplace le player
        jump = false;
   //     audioSourceWalk.clip = audioClipWalk;
    //    audioSourceWalk.Play();
    }
}
