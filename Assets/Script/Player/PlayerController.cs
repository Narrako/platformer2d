﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
	[SerializeField] private float m_JumpForce = 400f;  //la force ajouté quand le player saute
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  //De combien on rends le mouvement plus lisse
	[SerializeField] private bool m_AirControl = false;  //mettre le air control ou pas
	[SerializeField] private LayerMask m_WhatIsGround;  //le mask qui determine quel est celui du sol
	[SerializeField] private Transform m_GroundCheck;  //la position où il faut vérifier si le perso touche le sol
	
	#region Aduio Character

	[Header("Character Audio")] 
	[Space] 
	[SerializeField] protected AudioSource footstepAduioSource; //recup l'audio de déplacement
	
	private float timer;
	public float tiemBetweenStep;
	public GameObject audioManager;
	protected int currentFootstepSoundIndex = 0;
	
	#endregion

	#region Character Particles

	[Header("Character Particles")] 
	[Space] 
	[SerializeField] protected ParticleSystem runParticles;

	#endregion

	
	const float k_GroundedRadius = .2f; //radius de l'overlap circle pour déterminer s'il touche le sol
	private bool m_Grounded;  //mettre si le player touche ou pas le sol
	private Rigidbody2D m_Rigidbody2D;
	private bool m_FacingRight = true;  //pour déterminser dans quel sens le player regarde
	private Vector3 m_Velocity = Vector3.zero;

	private Animator anim;

	[Header("Events")]
	[Space]
	public UnityEvent OnLandEvent;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	private void Awake()
	{
		m_Rigidbody2D = GetComponent<Rigidbody2D>();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();

		anim = GetComponent<Animator>();

		currentFootstepSoundIndex = 0;
	}

	private void FixedUpdate()
	{
		bool wasGrounded = m_Grounded;
		m_Grounded = false;

		//Le Player est "grounded" si le circlecast de l'endroit où ya le groundcheck trouche quelque chose étant dessigné comme le sol
		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
			{
				m_Grounded = true;
				if (!wasGrounded)
					OnLandEvent.Invoke();
			}
		}
		anim.SetBool("Ground", m_Grounded);
		anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);  //set l'animation du mouvement vertical
		
		if (timer > 0)
			timer -= Time.deltaTime;
	}
	
	public void Move(float move, bool jump)
	{
		if(timer <= 0)
		{
			if (m_Grounded && move != 0) // joue le sons de déplacement à chaque fois que le player se déplace au sol
			{
				AudioManager.Singleton.PlayFootstepSound(footstepAduioSource, ref currentFootstepSoundIndex);
				timer = tiemBetweenStep;
			}
		}

		if (move != 0 && m_Grounded)
		{
			runParticles.Play();
			Debug.Log(("particles"));
		}
		else
			runParticles.Stop();
		


		if (m_Grounded || m_AirControl)
		{
			anim.SetFloat("Speed", Mathf.Abs(move));
			
			Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y); //déplace le personnage
			m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);  //ajouter l'effet smooth
			
			if (move > 0 && !m_FacingRight)  //si l'input de déplacement du player est droite et qu'il regarde à gauche
			{
				Flip(); //retourner le player
			}

			else if (move < 0 && m_FacingRight)  //a l'inverse si l'input est vers la gauche et qu'il regarde à droite
			{
				Flip(); //retourner le player
			}
		}
		if (m_Grounded && jump)  //si le player doit sauter
		{
			anim.SetBool("Ground", false);
			
			m_Grounded = false;
			m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));  //ajouter une force vertical au player
		}
	}

	private void Flip()
	{
		m_FacingRight = !m_FacingRight;  //change le sens où le player regarde

		//Multiplie la taille du player sur x par -1
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	private void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag("Coins"))
		{
			collider.GetComponent<SpriteRenderer>().enabled = false;
			collider.gameObject.transform.GetChild(0).gameObject.SetActive(true);
			StartCoroutine(DestroyCoin());
			Destroy(collider);
		}
	}
	
	IEnumerator DestroyCoin()
	{
		yield return new WaitForSeconds(1f);
	}
}
