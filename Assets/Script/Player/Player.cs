﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int fallBoundary = -20;
    public GameObject gameMatser;
    public Transform particlesPosition;
    
    public GameObject waterParticules;
    public Transform particlesWaterPosition;


    public void Start()
    {
        gameMatser = GameObject.FindWithTag("GameManager");
    }

   public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ennemis" && !Input.GetKeyDown(KeyCode.A))
        {
            Vector3 death = new Vector3(collision.transform.position.x - transform.position.x, 0, 0); //trouve l'endroit de collision avec l'ennemi
            particlesPosition.transform.position = death;
            DamagePlayer();
        }
    }

   public void OnTriggerEnter2D(Collider2D collider)
   {
       if(collider.gameObject.tag == "Water")
           Instantiate(waterParticules, particlesWaterPosition.position, particlesWaterPosition.rotation); //ajoute des particles d'eau
   }

   private void Update()
    {
        if (transform.position.y <= fallBoundary)
            DamagePlayer();
    }

    public void DamagePlayer()
    {
        gameMatser.GetComponent<GameMaster>().KillPlayer(this);
    }
}
