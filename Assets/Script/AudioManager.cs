﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager singleton;

    public static AudioManager Singleton
    {
        get { return singleton; }
    }

    public AudioSource audioSource;
    public AudioClip audioClip;

    [SerializeField] protected AudioClip[] footstepSound;

    private void Awake()
    {
        singleton = this;
    }

    public void PlaySoundOn(AudioSource audio, AudioClip clip) //pour jouer le sons qui correspond
    {
        audio.clip = clip;
        audio.Play();
    }

    public void PlayFootstepSound (AudioSource audio, ref int index) //pour le sons de quand le perso se déplace
    {
        if (footstepSound.Length > 0) 
        {
            PlaySoundOn (audio, footstepSound [index]);
            if (index < footstepSound.Length - 1)
                index++;
            else
                index = 0;
        }
    }

    public void playClip() //quand on appuie sur un bouton
    {
        audioSource.clip = audioClip;
        audioSource.Play();
    }
}
