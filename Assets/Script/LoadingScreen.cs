﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    public GameObject loadingScreenUI, startUI;
    public Animator animLoading, animStart;
    public float delay;

    [System.Serializable]
    public struct RandomImageItem //pour chaque item
    {
        public Color color; //donner la couleur
        public Sprite sprite; //dpner le sprite qui va avec
    }

    [SerializeField] protected RandomImageItem[] randomItems;
    [SerializeField] protected Image colorImage;
    [SerializeField] protected Image patternImage;
    
    void Start()
    {
        animLoading = loadingScreenUI.GetComponent<Animator>(); //recup l'anim de l'écran de chargement
        animStart = startUI.GetComponent<Animator>();  //recup l'anim de l'UI du menu
        
        if (randomItems.Length > 0) //choisit un nombre random
        {
            int index = Random.Range(0, randomItems.Length); //recup le nombre random
            patternImage.sprite = randomItems[index].sprite; //remplace le sprite par celui selectionné
            Color newColor = randomItems[index].color; //recup la couleur du nombre random
            newColor.a = Color.white.a; //prend une couleur blanche
            colorImage.color = newColor; //remplace l'image par celle selectionner au hasard
            newColor.a = randomItems[index].color.a;
            //patternImage.color = newColor;
        }
        StartCoroutine(loadingScreen());
    }
    
    public IEnumerator loadingScreen()
    { 
        yield return new WaitForSeconds(5f); //attends 5s
        animLoading.SetBool("Open", true); //lance l'anim pour faire partir l'écran de chargement
        yield return new WaitForSeconds(delay); //attends le temps de l'anim
        animStart.SetBool("Open", true); //lance l'anim pour faire apparaitre l'UI du menu
    }
}